# auth_dd



## Сборка проекта

1. Запустить программу в корне проекта - start.bat
2. Если вдруг не получилось через bat-файл, можно попробовать через консоль сделать следующее:
    в корне проекта запустить команду ./gradlew run

## Расположение файлов
1. Реализация алгоритма - src/main/java/ripe_realization/RIPEMD_160.java
2. Файл с хеш-значениями - src/main/resources/com/example/ripemd/hash.json