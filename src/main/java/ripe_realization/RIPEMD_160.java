package ripe_realization;

public class RIPEMD_160 {

    private int h0 = 0x67452301;
    private int h1 = 0xefcdab89;
    private int h2 = 0x98badcfe;
    private int h3 = 0x10325476;
    private int h4 = 0xc3d2e1f0;

    private final int[] r1 = {
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
        7, 4, 13, 1, 10, 6, 15, 3, 12, 0, 9, 5, 2, 14, 11, 8,
        3, 10, 14, 4, 9, 15, 8, 1, 2, 7, 0, 6, 13, 11, 5, 12,
        1, 9, 11, 10, 0, 8, 12, 4, 13, 3, 7, 15, 14, 5, 6, 2,
        4, 0, 5, 7, 9, 12, 2, 10, 14, 1, 3, 8, 11, 6, 15, 13
    };

    private final int[] r2 = {
            5, 14, 7, 0, 9, 2, 11, 4, 13, 6, 15, 8, 1, 10, 3, 12,
            6, 11, 3, 7, 0, 13, 5, 10, 14, 15, 8, 12, 4, 9, 1, 2,
            15, 5, 1, 3, 7, 14, 6, 9, 11, 8, 12, 2, 10, 0, 4, 13,
            8, 6, 4, 1, 3, 11, 15, 0, 5, 12, 2, 13, 9, 7, 10, 14,
            12, 15, 10, 4, 1, 5, 8, 7, 6, 2, 13, 14, 0, 3, 9, 11
    };

    private final int[] s1 = {
            11, 14, 15, 12, 5, 8, 7, 9, 11, 13, 14, 15, 6, 7, 9, 8,
            7, 6, 8, 13, 11, 9, 7, 15, 7, 12, 15, 9, 11, 7, 13, 12,
            11, 13, 6, 7, 14, 9, 13, 15, 14, 8, 13, 6, 5, 12, 7, 5,
            11, 12, 14, 15, 14, 15, 9, 8, 9, 14, 5, 6, 8, 6, 5, 12,
            9, 15, 5, 11, 6, 8, 13, 12, 5, 12, 13, 14, 11, 8, 5, 6
    };

    private final int[] s2 = {
            8, 9, 9, 11, 13, 15, 15, 5, 7, 7, 8, 11, 14, 14, 12, 6,
            9, 13, 15, 7, 12, 8, 9, 11, 7, 7, 12, 7, 6, 15, 13, 11,
            9, 7, 15, 11, 8, 6, 6, 14, 12, 13, 5, 14, 13, 13, 7, 5,
            15, 5, 8, 11, 14, 14, 6, 14, 6, 9, 12, 9, 12, 5, 15, 8,
            8, 5, 12, 9, 12, 5, 14, 6, 8, 13, 6, 5, 15, 13, 11, 11
    };

    private int[] add(String str){
        int num=((str.length()+8)/64)+1;
        int[] strByte =new int[num*16];
        for(int i=0;i<num*16;i++){
            strByte[i]=0;
        }
        int i;
        for(i=0;i<str.length();i++){
            strByte[i>>2]|=str.charAt(i)<<((i%4)*8);
        }
        strByte[i>>2]|=0x80<<((i%4)*8);
        strByte[num*16-2]= str.length()*8;
        return strByte;
    }

    public String getRipeMD(String source){
        h0 = 0x67452301;
        h1 = 0xefcdab89;
        h2 = 0x98badcfe;
        h3 = 0x10325476;
        h4 = 0xc3d2e1f0;
        int a1, a2, b1, b2, c1, c2, d1, d2, e1, e2, t;
        int[] strByte = add(source);
        for (int i=0; i < strByte.length/16; i++){
            a1 = a2 = h0;
            b1 = b2 = h1;
            c1 = c2 = h2;
            d1 = d2 = h3;
            e1 = e2 = h4;
            for (int j=0; j < 80; j++){
                t = shift(a1 + f(j, b1, c1, d1) + strByte[i*16 + r1[j]] + K1(j), s1[j]) + e1;
                a1 = e1; e1 = d1; d1 = shift(c1, 10); c1 = b1; b1 = t;
                t = shift(a2 + f(79-j, b2, c2, d2) + strByte[i*16 + r2[j]] + K2(j), s2[j]) + e2;
                a2 = e2; e2 = d2; d2 = shift(c2, 10); c2 = b2; b2 = t;
            }
            t = h1 + c1 + d2; h1 = h2 + d1 + e2; h2 = h3 + e1 + a2; h3 = h4 + a1 + b2;
            h4 = h0 + b1 + c2; h0 = t;
        }
        return changeHex(h0) + changeHex(h1) + changeHex(h2) + changeHex(h3) + changeHex(h4);
    }

    private String changeHex(int a){
        StringBuilder str= new StringBuilder();
        for(int i=0;i<4;i++){
            str.append(String.format("%2s", Integer.toHexString(((a >> i * 8) % (1 << 8)) & 0xff)).replace(' ', '0'));
        }
        return str.toString();
    }

    private int shift(int x, int n)
    {
        return (x << n) | (x >>> (32 - n));
    }

    private int f(int j, int x, int y, int z){
        return switch (j / 16) {
            case 0 -> x ^ y ^ z;
            case 1 -> (x & y) | (~x & z);
            case 2 -> (x | ~y) ^ z;
            case 3 -> (x & z) | (y & ~z);
            case 4 -> x ^ (y | ~z);
            default -> throw new IllegalStateException("Unexpected value: " + j);
        };
    }

    private int K1(int j){
        return switch (j / 16){
            case 0 -> 0x00000000;
            case 1 -> 0x5a827999;
            case 2 -> 0x6ed9eba1;
            case 3 -> 0x8f1bbcdc;
            case 4 -> 0xa953fd4e;
            default -> throw new IllegalStateException("Unexpected value: " + j);
        };
    }

    private int K2(int j){
        return switch (j / 16){
            case 0 -> 0x50a28be6;
            case 1 -> 0x5c4dd124;
            case 2 -> 0x6d703ef3;
            case 3 -> 0x7a6d76e9;
            case 4 -> 0x00000000;
            default -> throw new IllegalStateException("Unexpected value: " + j);
        };
    }
}
