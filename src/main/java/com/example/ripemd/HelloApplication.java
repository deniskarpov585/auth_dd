package com.example.ripemd;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import userInterface.UI;

import java.io.IOException;

public class HelloApplication extends Application {

    private final UI frames = new UI();

    @Override
    public void start(Stage stage) throws IOException {
        BorderPane root = new BorderPane();
        root.setTop(frames.menuPanel(stage));
        Scene scene = new Scene(root, 550, 240);
        stage.setTitle("Main menu");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}