package userInterface;

import org.json.JSONArray;
import ripe_realization.RIPEMD_160;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class JsonReader {

    private final RIPEMD_160 ripemd = new RIPEMD_160();

    public void jsonWrite(String content, String password, String filePath) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String currentLine = reader.readLine();
            reader.close();
            String hash = ripemd.getRipeMD(content+password);
            Map<String, String> map1 = new HashMap<>();
            JSONArray jsonArray = null;
            if (currentLine != null) {
                jsonArray = new JSONArray(currentLine);
                if (!currentLine.contains(hash)) {
                    map1.put(content, hash);
                    jsonArray.put(map1);
                }
            } else{
                jsonArray = new JSONArray("[]");
                map1.put(content, hash);
                jsonArray.put(map1);
            }
            BufferedWriter writer = new BufferedWriter(new FileWriter(filePath));
            writer.write(jsonArray.toString());
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public String jsonRead(String filePath) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        String currentLine = reader.readLine();
        reader.close();
        return currentLine;
    }
}
