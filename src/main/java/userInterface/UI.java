package userInterface;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import ripe_realization.RIPEMD_160;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.atomic.AtomicReference;

public class UI {

    private final RIPEMD_160 ripemd = new RIPEMD_160();
    private final JsonReader jsonReader = new JsonReader();

    private String count = "0";
    private boolean diff = false;

    public MenuBar menuPanel(Stage stage) {
        MenuBar menuBar = new MenuBar();

        Menu hashMenu = new Menu("Хеширование");
        MenuItem fileMenuItem = new MenuItem("Файл");
        fileMenuItem.setOnAction(actionEvent -> fileHashCreate(stage));
        MenuItem textMenuItem = new MenuItem("Текстовое сообщение");
        textMenuItem.setOnAction(actionEvent -> textHashCreate(stage));
        hashMenu.getItems().addAll(fileMenuItem, textMenuItem);

        Menu checkHashMenu = new Menu("Проверка хеш-значений");
        MenuItem fileCheckMenuItem = new MenuItem("Файл");
        fileCheckMenuItem.setOnAction(actionEvent -> fileHashCheck(stage));
        MenuItem textCheckMenuItem = new MenuItem("Текстовое сообщение");
        textCheckMenuItem.setOnAction(actionEvent -> textHashCheck(stage));
        checkHashMenu.getItems().addAll(fileCheckMenuItem, textCheckMenuItem);

        Menu passMenu = new Menu("Пароль");
        MenuItem difficultyChangeMenuItem = new MenuItem("Изменить сложность");
        difficultyChangeMenuItem.setOnAction(actionEvent -> changeDifficultyPass(stage));
        passMenu.getItems().addAll(difficultyChangeMenuItem);

        Menu aboutMenu = new Menu("Справка");
        MenuItem aboutMenuItem = new MenuItem("О программе");
        aboutMenuItem.setOnAction(actionEvent -> about(stage));
        aboutMenu.getItems().addAll(aboutMenuItem);

        menuBar.getMenus().addAll(hashMenu, checkHashMenu, passMenu, aboutMenu);
        return menuBar;
    }

    private void fileHashCreate(Stage stage){
        BorderPane root = new BorderPane();
        root.setTop(menuPanel(stage));
        FileChooser fileChooser = new FileChooser();

        Label chooseFile = new Label("");
        chooseFile.setPadding(new Insets(20, 50, 25, 0));

        Button button = new Button("Выберите файл");
        AtomicReference<File> selectedFile = new AtomicReference<>(new File(""));
        button.setOnAction(e -> {
            selectedFile.set(fileChooser.showOpenDialog(stage));
            chooseFile.setText("Файл: " + selectedFile.get().getPath());
            chooseFile.setFont(new Font("Times New Roman", 12));
        });

        Label whereChooseFile = new Label("");
        whereChooseFile.setPadding(new Insets(20, 50, 25, 0));

        FileChooser whereFileChooser = new FileChooser();
        Button whereButton = new Button("Выберите файл для хранения");
        AtomicReference<File> selectedWhereFile = new AtomicReference<>(new File(""));
        whereButton.setOnAction(e -> {
            selectedWhereFile.set(whereFileChooser.showOpenDialog(stage));
            whereChooseFile.setText("Файл: " + selectedWhereFile.get().getPath());
            whereChooseFile.setFont(new Font("Times New Roman", 12));
        });

        Label passwordLabel = new Label("Парольная фраза");
        PasswordField passwordField = new PasswordField();
        passwordLabel.setPadding(new Insets(0, 60, 25, 0));
        HBox firstPassword = new HBox(passwordLabel, passwordField);

        Label repeatLabel = new Label("Подтверждение фразы");
        PasswordField repeatField = new PasswordField();
        repeatLabel.setPadding(new Insets(0, 20, 25, 0));
        HBox secondPassword = new HBox(repeatLabel, repeatField);

        Label messageLabel = new Label();

        Button buttonConfirm = new Button("Хешировать");
        buttonConfirm.setOnAction(e -> {
            if (chooseFile.getText().equals("")){
                messageLabel.setText("Файл не выбран");
                messageLabel.setPadding(new Insets(0, 0, 25, 120));
            }
            else if (!passwordField.getText().equals(repeatField.getText())){
                messageLabel.setText("Не совпадают парольные фразы");
                messageLabel.setPadding(new Insets(0, 0, 25, 60));
            }
            else if (passwordField.getText().length() < Integer.parseInt(count) &&
                    diff &&
                    (!passwordField.getText().matches("(.*[a-z]+.*)")
                            || !passwordField.getText().matches("(.*[A-Z]+.*)")
                            || !passwordField.getText().matches("(.*[0-9]+.*)"))){
                messageLabel.setText("Не удовлетворяет ограничениям");
            }
            else {
                messageLabel.setText("");
                messageLabel.setPadding(new Insets(0, 0, 25, 60));
                byte[] encoded = new byte[]{};
                try {
                    encoded = Files.readAllBytes(selectedFile.get().toPath());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                String hide = new String(encoded, StandardCharsets.UTF_8);
                if (whereChooseFile.getText().equals("")){
                    messageLabel.setText(ripemd.getRipeMD(hide+passwordField.getText()));
                }
                else {
                    Path resourcePath = selectedWhereFile.get().toPath();
                    jsonReader.jsonWrite(hide, passwordField.getText(), resourcePath.toString());
                }
            }
        });
        HBox buttons = new HBox(buttonConfirm);

        VBox filePanel = new VBox(button, chooseFile, whereButton, whereChooseFile, firstPassword, secondPassword, messageLabel, buttons);
        filePanel.setPadding(new Insets(50, 0, 0, 50));
        root.setCenter(filePanel);
        Scene scene = new Scene(root, 550, 450);
        stage.setTitle("File hashing");
        stage.setResizable(false);
        stage.setScene(scene);
        stage.show();
    }


    private void textHashCreate(Stage stage){
        BorderPane root = new BorderPane();
        root.setTop(menuPanel(stage));

        Label writeText = new Label("Введите текст");
        writeText.setPadding(new Insets(20, 50, 25, 0));

        TextArea textArea = new TextArea();
        textArea.setMaxWidth(450);
        textArea.setMaxHeight(150);

        Label whereChooseFile = new Label("");
        whereChooseFile.setPadding(new Insets(20, 50, 25, 0));

        FileChooser whereFileChooser = new FileChooser();
        Button whereButton = new Button("Выберите файл для хранения");
        AtomicReference<File> selectedWhereFile = new AtomicReference<>(new File(""));
        whereButton.setOnAction(e -> {
            selectedWhereFile.set(whereFileChooser.showOpenDialog(stage));
            whereChooseFile.setText("Файл: " + selectedWhereFile.get().getPath());
            whereChooseFile.setFont(new Font("Times New Roman", 12));
        });

        Label passwordLabel = new Label("Парольная фраза");
        PasswordField passwordField = new PasswordField();
        passwordLabel.setPadding(new Insets(0, 60, 25, 0));
        HBox firstPassword = new HBox(passwordLabel, passwordField);
        firstPassword.setPadding(new Insets(10, 0, 0, 0));

        Label repeatLabel = new Label("Подтверждение фразы");
        PasswordField repeatField = new PasswordField();
        repeatLabel.setPadding(new Insets(0, 20, 25, 0));
        HBox secondPassword = new HBox(repeatLabel, repeatField);

        Label messageLabel = new Label();
        messageLabel.setPadding(new Insets(0, 0, 25, 60));

        Button buttonConfirm = new Button("Хешировать");
        buttonConfirm.setOnAction(e -> {
            if (!passwordField.getText().equals(repeatField.getText())){
                messageLabel.setText("Не совпадают парольные фразы");
                messageLabel.setPadding(new Insets(0, 0, 25, 80));
            }
            else if (diff &&
                    (passwordField.getText().length() < Integer.parseInt(count)
                            || !passwordField.getText().matches("(.*[a-zа-я]+.*)")
                            || !passwordField.getText().matches("(.*[A-ZА-Я]+.*)"))){
                messageLabel.setText("Не удовлетворяет ограничениям");
            }
            else {
                if (whereChooseFile.getText().equals("")){
                    messageLabel.setText(ripemd.getRipeMD(textArea.getText()+passwordField.getText()));
                }
                else {
                    messageLabel.setText("");
                    Path resourcePath = selectedWhereFile.get().toPath();
                    jsonReader.jsonWrite(textArea.getText(), passwordField.getText(), resourcePath.toString());
                }
            }
        });
        HBox buttons = new HBox(buttonConfirm);

        VBox filePanel = new VBox(writeText, textArea, whereButton, whereChooseFile, firstPassword, secondPassword, messageLabel, buttons);
        filePanel.setPadding(new Insets(20, 0, 0, 50));
        root.setCenter(filePanel);
        Scene scene = new Scene(root, 550, 550);
        stage.setTitle("Text hashing");
        stage.setResizable(false);
        stage.setScene(scene);
        stage.show();
    }

    private void fileHashCheck(Stage stage){
        BorderPane root = new BorderPane();
        root.setTop(menuPanel(stage));
        FileChooser fileChooser = new FileChooser();

        Label chooseFile = new Label("");
        chooseFile.setPadding(new Insets(20, 50, 25, 0));

        Button button = new Button("Выберите файл");
        AtomicReference<File> selectedFile = new AtomicReference<>(new File(""));
        button.setOnAction(e -> {
            selectedFile.set(fileChooser.showOpenDialog(stage));
            chooseFile.setText("Файл: " + selectedFile.get().getPath());
            chooseFile.setFont(new Font("Times New Roman", 12));
        });

        Label whereChooseFile = new Label("");
        whereChooseFile.setPadding(new Insets(20, 50, 25, 0));

        FileChooser whereFileChooser = new FileChooser();
        Button whereButton = new Button("Выберите файл для хранения");
        AtomicReference<File> selectedWhereFile = new AtomicReference<>(new File(""));
        whereButton.setOnAction(e -> {
            selectedWhereFile.set(whereFileChooser.showOpenDialog(stage));
            whereChooseFile.setText("Файл: " + selectedWhereFile.get().getPath());
            whereChooseFile.setFont(new Font("Times New Roman", 12));
        });

        Label passwordLabel = new Label("Парольная фраза");
        PasswordField passwordField = new PasswordField();
        passwordLabel.setPadding(new Insets(0, 60, 25, 0));
        HBox firstPassword = new HBox(passwordLabel, passwordField);

        Label messageLabel = new Label();

        Button buttonConfirm = new Button("Проверить целостность");
        buttonConfirm.setOnAction(e -> {
            if (chooseFile.getText().equals("")){
                messageLabel.setText("Файл не выбран");
                messageLabel.setPadding(new Insets(0, 0, 25, 120));
            }
            else {
                byte[] encoded = new byte[]{};
                try {
                    encoded = Files.readAllBytes(selectedFile.get().toPath());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                Path resourcePath = selectedWhereFile.get().toPath();

                String jsonString = "";

                try {
                    jsonString = jsonReader.jsonRead(resourcePath.toString());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                if (jsonString.contains(ripemd.getRipeMD(new String(encoded, StandardCharsets.UTF_8) +
                        passwordField.getText()))){
                    messageLabel.setText("Проверка прошла успешно");
                }
                else {
                    messageLabel.setText("Ошибка целостности данных");
                }
                messageLabel.setPadding(new Insets(0, 0, 25, 60));
            }
        });
        HBox buttons = new HBox(buttonConfirm);

        VBox filePanel = new VBox(button, chooseFile, whereButton, whereChooseFile, firstPassword, messageLabel, buttons);
        filePanel.setPadding(new Insets(50, 0, 0, 50));
        root.setCenter(filePanel);
        Scene scene = new Scene(root, 550, 450);
        stage.setTitle("File checking");
        stage.setResizable(false);
        stage.setScene(scene);
        stage.show();
    }

    private void textHashCheck(Stage stage){
        BorderPane root = new BorderPane();
        root.setTop(menuPanel(stage));

        Label writeText = new Label("Введите текст");
        writeText.setPadding(new Insets(20, 50, 25, 0));

        TextArea textArea = new TextArea();
        textArea.setMaxWidth(450);
        textArea.setMaxHeight(150);

        Label whereChooseFile = new Label("");
        whereChooseFile.setPadding(new Insets(20, 50, 25, 0));

        FileChooser whereFileChooser = new FileChooser();
        Button whereButton = new Button("Выберите файл для хранения");
        AtomicReference<File> selectedWhereFile = new AtomicReference<>(new File(""));
        whereButton.setOnAction(e -> {
            selectedWhereFile.set(whereFileChooser.showOpenDialog(stage));
            whereChooseFile.setText("Файл: " + selectedWhereFile.get().getPath());
            whereChooseFile.setFont(new Font("Times New Roman", 12));
        });

        Label passwordLabel = new Label("Парольная фраза");
        PasswordField passwordField = new PasswordField();
        passwordLabel.setPadding(new Insets(0, 60, 25, 0));
        HBox firstPassword = new HBox(passwordLabel, passwordField);
        firstPassword.setPadding(new Insets(10, 0, 0, 0));

        Label messageLabel = new Label();
        messageLabel.setPadding(new Insets(0, 0, 25, 60));

        Button buttonConfirm = new Button("Проверить целостность");
        buttonConfirm.setOnAction(e -> {
            Path resourcePath = selectedWhereFile.get().toPath();

            String jsonString = "";

            try {
                jsonString = jsonReader.jsonRead(resourcePath.toString());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (jsonString.contains(ripemd.getRipeMD(textArea.getText() +
                    passwordField.getText()))){
                messageLabel.setText("Проверка прошла успешно");
            }
            else {
                messageLabel.setText("Ошибка целостности данных");
            }
            messageLabel.setPadding(new Insets(0, 0, 15, 80));
        });
        HBox buttons = new HBox(buttonConfirm);

        VBox filePanel = new VBox(writeText, textArea, whereButton, whereChooseFile, firstPassword, messageLabel, buttons);
        filePanel.setPadding(new Insets(20, 0, 0, 50));
        root.setCenter(filePanel);
        Scene scene = new Scene(root, 550, 500);
        stage.setTitle("Text checking");
        stage.setResizable(false);
        stage.setScene(scene);
        stage.show();
    }

    private void changeDifficultyPass(Stage stage){
        BorderPane root = new BorderPane();
        root.setTop(menuPanel(stage));

        Label passwordLabel = new Label("Количество символов");
        TextField passwordField = new TextField(count);
        passwordLabel.setPadding(new Insets(0, 60, 25, 0));
        HBox password = new HBox(passwordLabel, passwordField);
        password.setPadding(new Insets(25, 0, 0, 0));

        Label difficultyLabel = new Label("Обязательное наличие строчных и прописных букв");
        CheckBox difficultyBox = new CheckBox();
        difficultyBox.setSelected(diff);
        difficultyLabel.setPadding(new Insets(0, 20, 25, 0));
        HBox checkBox = new HBox(difficultyLabel, difficultyBox);

        Label messageLabel = new Label();
        messageLabel.setPadding(new Insets(0, 0, 25, 60));

        Button buttonConfirm = new Button("Применить");
        buttonConfirm.setOnAction(e -> {
            if (isNumeric(passwordField.getText())){
                count = passwordField.getText();
            } else {
                messageLabel.setText("Некорректные данные");
            }
            diff = difficultyBox.isSelected();
        });
        HBox buttons = new HBox(buttonConfirm);

        VBox filePanel = new VBox(password, checkBox, messageLabel, buttons);
        filePanel.setPadding(new Insets(20, 0, 0, 50));
        root.setCenter(filePanel);
        Scene scene = new Scene(root, 550, 300);
        stage.setTitle("Difficulty password");
        stage.setResizable(false);
        stage.setScene(scene);
        stage.show();
    }

    private static boolean isNumeric(String str) {
        return str.matches("\\d+(\\.\\d+)?");
    }

    private void about(Stage stage){
        BorderPane root = new BorderPane();
        root.setTop(menuPanel(stage));

        Label authorLabel = new Label("А-05-19 Карпов Денис");
        authorLabel.setPadding(new Insets(0, 0, 0, 150));
        Label titleLabel = new Label("Программная реализация функции хеширования RIPEMD");
        titleLabel.setPadding(new Insets(0, 0, 0, 25));

        VBox filePanel = new VBox(authorLabel, titleLabel);
        filePanel.setPadding(new Insets(20, 0, 0, 50));
        root.setCenter(filePanel);
        Scene scene = new Scene(root, 550, 100);
        stage.setTitle("About");
        stage.setResizable(false);
        stage.setScene(scene);
        stage.show();
    }
}
