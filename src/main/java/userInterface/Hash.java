package userInterface;

public class Hash {

    private String content;
    private String hash;

    public Hash(){
    }

    public Hash(
            String content,
            String hash){
        this.content = content;
        this.hash = hash;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
