module com.example.ripemd {
    requires javafx.controls;
    requires javafx.fxml;
    requires com.fasterxml.jackson.databind;
    requires org.json;

    opens com.example.ripemd to javafx.fxml;
    exports com.example.ripemd;
}